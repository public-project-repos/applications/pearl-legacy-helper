import java.io.File
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import kotlinx.serialization.decodeFromString

@Serializable
data class ConfigData(
    val `PEARL-FKT-INCL`: PearlfktIncl
)

@Serializable
data class PearlfktIncl(
    val InputFile: String,
    val OutputFile: String,
    val Pattern_begin: String,
    val Pattern_end: String
)

fun main(args: Array<String>) {
    if (args.size != 1) {
        println("Verwendung: pearl-legacy-helper.jar <config.json>")
        return
    }
    val config_json = File(args[0])
    val json_string = config_json.readText()

    val configData = Json.decodeFromString<ConfigData>(json_string)
    val file_to_scan = File(configData.`PEARL-FKT-INCL`.InputFile)
    val file_to_copy_found_content = File(configData.`PEARL-FKT-INCL`.OutputFile)
    val start_pattern = configData.`PEARL-FKT-INCL`.Pattern_begin
    val end_pattern = configData.`PEARL-FKT-INCL`.Pattern_end


    val text_to_search = file_to_scan.readText()
    val cp_start_pos = text_to_search.indexOf(start_pattern)
    val cp_end_pos = text_to_search.indexOf(end_pattern, cp_start_pos)

    if (cp_start_pos != -1 && cp_end_pos != -1) {
        val copy_content = text_to_search.substring(cp_start_pos, cp_end_pos + end_pattern.length)
        file_to_copy_found_content.writeText(copy_content)
        println("Inhalt erfolgreich kopiert und in $file_to_copy_found_content gespeichert.")
    } else {
        println("String-Pattern ist nicht in der Datei!")
    }
}